from rest_framework.routers import DefaultRouter
from listings.views import ListingBookedDatesViewSet, BookingInfoViewSet
from django.conf.urls import url, include

router = DefaultRouter()

router.register(r'booking_info', BookingInfoViewSet)
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^units/', ListingBookedDatesViewSet.as_view()),
]
