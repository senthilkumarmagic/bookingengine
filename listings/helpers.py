from datetime import datetime


class Helpers:

    def __init__(self):
        pass

    def convert_date_format(self, input_date):
        return datetime.strptime(input_date, '%Y-%m-%d').date()

    def check_required_param_exist(self, request, required_params):
        for param in required_params:
            if param not in request.query_params:
                return False

        return True
