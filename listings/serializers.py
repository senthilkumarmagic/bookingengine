from rest_framework import serializers

from listings.models import ListingBookedDates, Listing, BookingInfo


class ListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Listing
        fields = ('listing_type', 'title', 'country', 'city')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data


class BookingInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookingInfo
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data


class ListingBookedDatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListingBookedDates
        fields = ('id', 'booking_info', 'check_in_date', 'check_out_date')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data
