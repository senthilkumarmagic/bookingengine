

from rest_framework import viewsets, status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist

from listings.models import ListingBookedDates, BookingInfo, HotelRoom, HotelRoomType, BookingInfo
from listings.serializers import ListingBookedDatesSerializer, ListingSerializer, BookingInfoSerializer

from datetime import datetime, timedelta
from django.db.models import Q

from listings.helpers import Helpers


class BookingInfoViewSet(viewsets.ModelViewSet):
    queryset = BookingInfo.objects.all()
    serializer_class = BookingInfoSerializer

    def get_permissions(self):
        if self.request.method == 'GET':
            self.permission_classes = [AllowAny]
        else:
            self.permission_classes = [IsAdminUser]
        return [permission() for permission in self.permission_classes]


class ListingBookedDatesViewSet(GenericAPIView):
    permission_classes = [AllowAny]
    queryset = ListingBookedDates.objects.all()
    serializer_class = ListingBookedDatesSerializer

    def post(self, request):
        try:
            helper = Helpers()
            required_param = ['booking_id', 'check_in', 'check_out']
            if not helper.check_required_param_exist(self.request, required_param):
                return Response({"message": "Incorrect input parameters"}, status=status.HTTP_200_OK)

            booking_id = self.request.query_params.get('booking_id')
            check_in = helper.convert_date_format(self.request.query_params.get('check_in'))
            check_out = helper.convert_date_format(self.request.query_params.get('check_out'))

            booking_info_handler = BookingInfo.objects.get(id=booking_id)
            booking = ListingBookedDates(booking_info=booking_info_handler, check_in_date=check_in, check_out_date=check_out)
            booking.save()

            return Response({"message": "Booked successfully", "items": ListingBookedDatesSerializer(booking).data},
                            status=status.HTTP_200_OK)

        except ValueError as err:
            return Response({"message": "Incorrect input parameters"}, status=status.HTTP_200_OK)
        except ObjectDoesNotExist as err:
            return Response({"message": "Incorrect booking id"}, status=status.HTTP_200_OK)

    def get(self, request):
        try:
            helper = Helpers()
            required_param = ['max_price', 'check_in', 'check_out']
            if not helper.check_required_param_exist(self.request, required_param):
                return Response({"message": "Incorrect input parameters"}, status=status.HTTP_200_OK)

            max_price = self.request.query_params.get('max_price')
            check_in = helper.convert_date_format(self.request.query_params.get('check_in'))
            check_out = helper.convert_date_format(self.request.query_params.get('check_out'))

            accoms_below_max_price = BookingInfo.objects.filter(price__lte=max_price).order_by('price')

            available_accom_data = []
            hotel_room_type_record = []
            for accom in accoms_below_max_price:
                accom_details = {}
                if accom.listing is not None:
                    accom_details = self.get_available_apartments(accom, check_in, check_out)

                if accom.hotel_room_type is not None:
                    if accom.hotel_room_type.hotel.id not in hotel_room_type_record:  # skip hotel - prior cheaper room
                        hotel_room_id, accom_details = self.get_available_hotel(accom, check_in, check_out)
                        if hotel_room_id is not None:
                            hotel_room_type_record.append(hotel_room_id)

                if bool(accom_details):
                    available_accom_data.append(accom_details)

            return Response({"items": available_accom_data}, status=status.HTTP_200_OK)

        except ValueError as err:
            return Response({"message": "Incorrect input parameters"}, status=status.HTTP_200_OK)

    def check_overlapping_booked_accomodations(self, blocked_dates_query, check_in, check_out, booking_info_accom):
        return blocked_dates_query.filter(
            Q(Q(check_in_date__lte=check_in) & Q(check_out_date__gte=check_in)) |
            Q(Q(check_in_date__lt=check_out) & Q(check_out_date__gte=check_out)),
            booking_info=booking_info_accom)

    def get_available_apartments(self, accom, check_in, check_out):
        booked_apt = self.check_overlapping_booked_accomodations(self.queryset, check_in, check_out, accom)
        accom_details = {}
        if len(booked_apt) <= 0:
            accom_details = ListingSerializer(accom.listing).data
            accom_details['price'] = accom.price

        return accom_details

    def get_available_hotel(self, accom, check_in, check_out):
        accom_details = {}
        query_room_numbers = HotelRoom.objects.filter(hotel_room_type=accom.hotel_room_type).\
            values_list('room_number', flat=True)
        number_of_rooms = len(list(query_room_numbers))

        booked_hotels = self.check_overlapping_booked_accomodations(self.queryset, check_in, check_out,
                                                                    accom)
        rooms_remaining = number_of_rooms - len(booked_hotels)

        if len(booked_hotels) <= 0 or rooms_remaining > 0:
            accom_details = ListingSerializer(accom.hotel_room_type.hotel).data
            accom_details['price'] = accom.price
            return accom.hotel_room_type.hotel.id, accom_details

        return None, accom_details
