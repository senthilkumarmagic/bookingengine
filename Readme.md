# Django & Django REST framework test project. by Senthil Kumar

## Additional notes:
 - No specific authentication (Ex: JWT) was implemented to access the API

## Summary:

1. There is a pre-build structure for Hotels/Apartments (could be changed or extended). Database is prefilled with information - **db.sqlite3**.
    - superuser
        - username: **admin**
        - password: **admin**

2. Endpoint to **block days** ( make reservations ) for each **Apartment** or **HotelRoom** - compeleted.
    - new model for blocked dates created - ListingBookedDates
    - Example endpoint to do booking: POST http://127.0.0.1:8000/api/v1/units/?booking_id=6&check_in=2021-05-24&check_out=2021-05-25
    - Incorrect booking_id will result in "incorrect booking id message" response
    ## Response example:
          {
              "message": "Booked successfully",
              "items": {
                  "id": 6,
                  "booking_info": 5,
                  "check_in_date": "2021-05-24",
                  "check_out_date": "2021-05-25"
              }
         }
    
    
    
    - Booking_id can be retrieved from this endpoint that retrieves all the accomodations in BookingInfo: GET http://localhost:8000/api/v1/booking_info/
    - Only GET request can be done, POST request is limited to AdminUser only
    ## Response example:
        [
            {
                "id": 1,
                "price": "40.00",
                "listing": 1,
                "hotel_room_type": null
            },
            {
                "id": 2,
                "price": "90.00",
                "listing": 2,
                "hotel_room_type": null
            },
        ]
    - Please take note that the booking endpoint does not check if the room/apartment is available - keeping it simple based on the requirement



3. NEW **endpoint** where we will get available Apartments and Hotels based on - completed:
	- **available days** (date range ex.: "from 2021-12-09 to 2021-12-12")
            - Apartment should not have any blocked day inside the range
            - Hotel should have at least 1 Hotel Room available from any of the HotelRoomTypes
     - **max_price** (100):
		- Apartment price must be lower than max_price.
		- Hotel should have at least 1 Hotel Room without any blocked days in the range with price lower than max_price.

	- returned objects should be **sorted** from lowest to highest price.
		-  hotels should display the price of the **cheapest HotelRoomType** with **available HotelRoom**.
    
    - New endpoint created, example: http://localhost:8000/api/v1/units/?max_price=100&check_in=2021-12-09&check_out=2021-12-12
    - The response will be exactly the same as the response example shown below.
    - 


## Initial Project setup
    git clone https://bitbucket.org/staykeepersdev/bookingengine.git
    python -m venv venv
    pip install -r requirements.txt
    python manage.py runserver


## Test Case example:

For covering more test cases we are going to need at least one hotel with 3 Hotel Room Types:

- First with price=50 (below max_price) with blocked day inside the search criteria for all rooms(could be 1 room)

- Second with price=60 (below max_price) with blocked day insde the search criteria for one out of few rooms

- Third with price 200 (above max_price) 


## Request example:

http://localhost:8000/api/v1/units/?max_price=100&check_in=2021-12-09&check_out=2021-12-12


## Response example:

    {
        "items": [
            {
                "listing_type": "Apartment",
                "title": "Luxurious Studio",
                "country": "UK",
                "city": "London",
                "price": "40"

            },
            {
                "listing_type": "Hotel",
                "title": "Hotel Lux 3***",
                "country": "BG",
                "city": "Sofia",
                "price": "60" # This the price of the first Hotel Room Type with a Room without blocked days in the range

            },
            {
                "listing_type": "Apartment",
                "title": "Excellent 2 Bed Apartment Near Tower Bridge",
                "country": "UK",
                "city": "London",
                "price": "90"
            },
        ]
    }